# Virtual RMS – Bash

Implementação em shell/bash do programa VRMS (Virtual RMS), atualmente [empacotado pelo Projeto Debian](https://salsa.debian.org/debian/check-dfsg-status) com o nome de `check-dfsg-status`.

[![asciicast](https://asciinema.org/a/570278.svg)](https://asciinema.org/a/570278)

Este script limita-se a verificar os pacotes instalados no Debian a partir dos repositórios `non-free` e `contrib`, sem qualquer verificação adicional. Para isso, ele utiliza o programa `dpkg-query`, que oferece várias formas de consulta à base de dados do `dpkg`.

## Atualização!

Script atualizado para a nova separação de repositórios do Debian:

![](vrms-bash.png)

## Aviso!

Este script foi criado para uso pessoal e é compartilhado apenas como uma demonstração das aplicações práticas da programação do Bash.

## Uso

```
vrms [OPÇÕES]
```

## Opções

```
-p      Imprime a lista dos malwares encontrados.
-h      Ajuda.
-v      Versão e licença.
```

